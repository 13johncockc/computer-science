import java.util.Scanner;

public class Main {

    private static Scanner scanner;

    public static void main(String[] args) {
        Main.scanner = new Scanner(System.in);

        String firstName;
        String secondName;


        System.out.println("Please enter your first name");
        firstName = scanner.nextLine();
        System.out.println("Please enter your second name");
        secondName = scanner.nextLine();


        String firstSwap = firstName.substring(0,2);
        String secondSwap = secondName.substring(0,2);


        firstName = secondSwap + firstName.substring(2, firstName.length());
        secondName = firstSwap + secondName.substring(2, secondName.length());


        System.out.println("Hello " + firstName + " " + secondName);
    }


}
