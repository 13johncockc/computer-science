import java.util.Scanner;

public class Main {

    private static Scanner scanner;

    public static void main(String[] args) {
        scanner = new Scanner(System.in);

        check();
    }

    private static void check() {
        System.out.println("Please input 3 integers");
        int firstNumber = scanner.nextInt();
        int secondNumber = scanner.nextInt();
        int thirdNumber = scanner.nextInt();

        if (firstNumber > secondNumber && firstNumber > thirdNumber) {
            System.out.print("The first number " + firstNumber + " was bigger");
        }else if (secondNumber > thirdNumber && secondNumber > firstNumber) {
            System.out.print("The second number " + secondNumber + " was bigger");
        }else if (thirdNumber > secondNumber && thirdNumber > firstNumber) {
            System.out.print("The third number " + thirdNumber + " was bigger");
        }else{
            System.out.print("All three numbers are equal");
        }

    }


}
