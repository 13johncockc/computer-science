import javax.swing.*;
import java.awt.*;

public class RegPanel extends JPanel {

    public RegPanel(String reg) {
        setBackground(Color.YELLOW);

        JLabel label = new JLabel(reg);

        JButton button = new JButton("New");

        button.addActionListener((act) -> {
            label.setText("<html><p style='font-size: 75px;font-family: Arial;'>" + Main.randomPlate() + "</p><b></html>");
            repaint();
        });

        setLayout(null);

        label.setBounds(0, 0, 500, 200);
        button.setBounds(200, 225, 100, 25);
        add(label);
        add(button);
    }
}
