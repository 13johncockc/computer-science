import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

    private static Random random;

    public static void main(String[] args) {
        random = new Random();

        JFrame frame = new JFrame();
        frame.setLocationRelativeTo(null);
        frame.setSize(500, 300);
        frame.add(new RegPanel(randomPlate()));
        frame.setVisible(true);


    }

    public static String randomPlate() {
        String[] letters = new String[5];
        int[] numbers = new int[3];

        for (int i = 0; i < letters.length; i++) {
            char c = (char) (random.nextInt(26) + 'a');
            letters[i] = Character.toString(c);
        }

        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = random.nextInt(9);
        }

        String plate = letters[0] + letters[1] + numbers[0] + numbers[1] + numbers[2] + letters[2] + letters[3] + letters[4];


        return plate;
    }


}
