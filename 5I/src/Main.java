import java.util.Random;
import java.util.Scanner;

public class Main {

    private static Scanner scanner;
    private static Random random = new Random();

    public static void main(String[] args) {
        Main.scanner = new Scanner(System.in);
        call();
    }

    public static void call() {
        int randomNumber = random.nextInt(100);

        int input;

        boolean guessed = false;

        int tries = 0;

        System.out.println(randomNumber);
        while (!guessed) {
            System.out.print("Please enter your guess > ");
            input = scanner.nextInt();

            tries++;
            if (randomNumber == input) {
                guessed = true;
                break;
            }

            String hl = (randomNumber < input) ? " HIGH" : " LOW";
            System.out.println("You guess is too " + hl );
        }


        System.out.println("Congratulations you have guessed the number in " + tries);
    }

}
