import java.util.Random;

public class Main {

    public static void main(String[] args) {
        Random random = new Random();

        int heads = 0;

        int tries = 0;
        while (heads != 3) {
            tries++;
            int x = random.nextInt(2);
            if (x == 1) {
                System.out.println("Heads");
                heads++;
            }else{
                System.out.println("Tails");
                heads = 0;
            }
        }

        System.out.println("Three heads in a row in " + tries + " tries");
    }

}
