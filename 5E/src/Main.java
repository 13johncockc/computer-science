import java.util.Random;

public class Main {

    public static void main(String[] args) {

        Random random = new Random();

        int count = 0;

        int x = random.nextInt(100);

        while(x != 55) {
            count++;
            System.out.println(x);
            x = random.nextInt(100);
        }

        System.out.println(x);

        System.out.println("Completed the task in " + count + " tries");
    }

}
