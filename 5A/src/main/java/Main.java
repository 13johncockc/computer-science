import java.util.Scanner;

public class Main {

    private static Scanner scanner;

    public static void main(String[] args) {
        scanner = new Scanner(System.in);

        System.out.print("Please enter your sentence > ");
        String sentance = scanner.nextLine();

        int found = 0;

        System.out.print("Please enter a character to find > ");
        char character = scanner.nextLine().charAt(0);

        for (char cha : sentance.toCharArray()) {
            if (character == cha) {
                found++;
            }
        }

        System.out.print("Number of '" + character +"'s found > " + found);
    }
}
