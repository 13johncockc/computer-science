import java.util.Random;
import java.util.Scanner;

public class Main {

    private static Scanner scanner;
    private static Random random;

    private static int range = 1000;

    public static void main(String[] args) {
//        scanner = new Scanner(System.in);
//        random = new Random();
//
//        System.out.println("Welcome to Maths Trainer!");
//        check();

    }

    private static void menu() {
        System.out.print("Do you want another question? (Y/N) ");
        String input = scanner.next();
        if (input.equalsIgnoreCase("Y")) {
            check();
        } else {
            System.exit(0);
        }
    }

    private static void check() {
        int randomOne = random.nextInt(range);
        int randomTwo = random.nextInt(range);
        int expected = randomOne + randomTwo;

        System.out.print("What is " + randomOne + " + " + randomTwo + "? (" + expected + ") ");

        if (scanner.nextInt() == expected) {
            System.out.println("Correct!");
        } else {
            System.out.println("Incorrect!");
        }

        menu();
    }

}
