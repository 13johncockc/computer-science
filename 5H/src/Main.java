import java.util.Scanner;

public class Main {

    private static Scanner scanner;

    public static void main(String[] args) {

        Main.scanner = new Scanner(System.in);

        call();

        boolean ended = false;

        while (!ended) {
            System.out.println("Would you like to try another (Y/N)");
            String input = scanner.nextLine();

            if (input.equalsIgnoreCase("y")) {
                ended = false;
            }else{
                ended = true;
            }

            if (!ended) {
                call();
            }
        }

    }

    private static void call() {
        System.out.print("Please enter a 5 digit number > ");

        String result = scanner.nextLine();

        if (result.length() != 5) {
            return;
        }

        int number = Integer.parseInt(result);

        if ((number % 2) == 0) {
            System.out.println("Number is not Odd");
        } else {
            System.out.println("Number is very odd");
        }
    }


}
