import java.util.Date;
import java.util.Scanner;

public class Main {

    private static Scanner scanner;

    public static void main(String[] args) {
        scanner = new Scanner(System.in);
        System.out.print("Please enter a month ");
        int month = scanner.nextInt();
        System.out.print("Please enter a day ");
        int day = scanner.nextInt();

        switch (month) {
            case 1:
                System.out.println("You are in winter");
                break;
            case 2:
                System.out.println("You are in winter");
                break;
            case 3:
                if (day <= 15) {
                    System.out.println("You are in winter");
                } else {
                    System.out.println("You are in spring");
                }
                break;
            case 4:
                System.out.println("You are in spring");
                break;
            case 5:
                System.out.println("You are in spring");
                break;
            case 6:
                if (day <= 15) {
                    System.out.println("You are in spring");
                } else {
                    System.out.println("You are in summer");
                }
                break;
            case 7:
                System.out.println("You are in summer");
                break;
            case 8:
                System.out.println("You are in summer");
                break;
            case 9:
                if (day <= 15) {
                    System.out.println("You are in summer");
                } else {
                    System.out.println("You are in autumn");
                }
                break;
            case 10:
                System.out.println("You are in autumn");
                break;
            case 11:
                System.out.println("You are in autumn");
                break;
            case 12:
                if (day <= 15) {
                    System.out.println("You are in autumn");
                } else {
                    System.out.println("You are in winter");
                }
                break;
        }


    }
}
